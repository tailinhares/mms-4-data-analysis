# MMS 4 - Data Analysis
This repository contains the datasets and jupyter notebook from the experiments made by the Group 4 during the seminar "Grundlagen der Mensch-Maschinen Systeme" SoSe2021 at TU Berlin.

## How to use it:
1. Clone or Download the repository.
2. Install Python dependencies from the **`requirements.txt`** file.
3. Run the **`mms4_data_analysis.ipynb`** on Jupyter Notebook.

## Research:
The Deutsche Bahn (DB) has three booking experiences, the app, the website, and the ticket machine (Automat). As part of the seminar “Human-Machine Systems” at TU Berlin we were asked to design and deploy a user experience test using a variety of methods.

### Operationalization:
**Research Question**: which DB booking experience generates higher user satisfaction?

**Hypothesis**: The user satisfaction is higher with the website, because it is more user friendly, it is easier to use, and less stressful.

**Metrics**: 
- User satisfaction: Customer Satisfaction Score (CSAT-score).
- System usability: System Usability Scale (SUS).
- Interaction speed: Clicks/Duration.
- Stress: Electrodermal Activity (EDA).

### Experiment:

**Setting**:
- Study design: within-subjects.
- Field experiment.
- Sample size: 10 participants.

**Participants Demographics**:
- Sample is not representative, mostly male, young and highly educated.

1. 60% of participants are men.

2. 80% of participants are between 21-29 years old.

3. 50% are bachelor students.

4. 70% are students in general, 20% work, and only one person is currently looking for a job.

5. Participants book tickets more often on the Website than at the ticket machine.

![Booking frequency with each system](/Plots/nutzungshaufigkeit.png)


### Results:

1. SUS for both systems are NOT significantly different. However, according to this [study](https://measuringu.com/sus/), an average SUS greater than 68 is considered above the average. For this reason, **the ticket machine and website usability are both above the average**.

2. CSAT scores are NOT significantly different. Nonetheless, **the percentage of participants satisfied with the Website is higher (90%) than with the Automat (80%)**.

3. Interaction speeds are NOT significantly different.

4. Average task deployment durations are NOT significantly different either.

5. **Only average clicks per task are significantly different between both systems**.

6. **SUS and CSAT scores positive strongly** correlate for both Automat and Website.

7. **SUS and CSAT scores positive strongly correlate with interaction speed for Website**.

8. **SUS and CSAT scores negative strongly correlate with average interaction duration for Website**.





